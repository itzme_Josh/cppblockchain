//#ifndef LISTENER_H    // To make sure you don't declare the function more than once by including the header multiple times.
#define LISTENER_H
#define MYPORT "4950" // the port users will be connecting to
#define MAXBUFLEN 1000

#include <iostream>
#include <arpa/inet.h>         // for inet_ntop
#include <netdb.h>             // for addrinfo, freeaddrinfo, gai_strerror
#include <netinet/in.h>        // for INET6_ADDRSTRLEN, sockaddr_in, sockadd...
#include <stdio.h>             // for printf, fprintf, perror, NULL, stderr
#include <stdlib.h>            // for exit
#include <string.h>            // for memset
#include <sys/socket.h>        // for sockaddr_storage, bind, socket, AF_INET
#include <unistd.h>            // for close
#include <string>
#include <vector>
#include <thread>
#include "json.hpp"
#include "block.h"
#include <poll.h>
using namespace std;

// for convenience
using json = nlohmann::json;

void connectToPeers(const std::vector<const char *>&);
int initHttpServer(const std::vector<const char *>&);
void initP2PServer(const std::vector<std::string>&);
void listen();

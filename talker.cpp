#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>  
//The header file socket.h includes a number of definitions of structures needed for sockets.
#include <netinet/in.h>  
//The header file netinet/in.h contains constants and structures needed for internet domain addresses.
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <memory.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <errno.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>


std::vector<int> addresses = {4951,4952,4953,4954,4955};


void sendBlock(const std::string &data)
{

  for(int n : addresses) {
    int s;
    int ret;
    char *buf;
    struct sockaddr_in addr; //socket address for internet, defined in netinet I think
    //A sockaddr_in is a structure containing an internet address. This structure is defined in <netinet/in.h>.
   
    /*if (argc != 4) {
      puts("usage: send ipaddr port data");
      exit(1);
    }*/
   
    addr.sin_family = AF_INET;
    //inet_aton Convert IP addresses from a dots-and-number string to a struct in_addr and back
    // arg[1] is the address input 
    ret = inet_aton("192.168.0.11", &addr.sin_addr);
    if (ret == 0) { perror("inet_aton"); exit(1); }  // I assume inet_aton returns 0, exit
    addr.sin_port = htons(n);  // (host to network short)
    //htons - Convert multi-byte integer types from host byte order to network byte order 
    // Port number

    //buffer? words to be sent.

    //char *cstr = data.c_str();
    char* chr = strdup(data.c_str());
    buf = chr;//"{\"message\":\"NewBlock\",\"index\":\"1\",\"previousHash\":\"9191837481\",\"data\":\"helllo\",\"hash\":\"0192829290\", \"timestamp\":\"1506931164\"}"; 
   

   /*
  The socket() system call creates a new socket. 
  It takes three arguments. The first is the address domain of the socket. 
  Recall that there are two possible address domains, the unix domain for two processes which share a common file system, and the Internet domain for any two hosts on the Internet. 
  The symbol constant AF_UNIX is used for the former, and AF_INET for the latter (there are actually many other options which can be used here for specialized purposes).
  The second argument is the type of socket. 
  Recall that there are two choices here, a stream socket in which characters are read in a continuous stream as if from a file or pipe, and a datagram socket, in which messages are read in chunks. The two symbolic constants are SOCK_STREAM and SOCK_DGRAM. The third argument is the protocol. If this argument is zero (and it always should be except for unusual circumstances), the operating system will choose the most appropriate protocol. It will choose TCP for stream sockets and UDP for datagram sockets.
   */

  //new socket, (x, ntype of socket, and protocol)
    s = socket(PF_INET, SOCK_DGRAM, 0);
    if (s == -1) { perror("socket"); exit(1); }

    /*
    ssize_t sendto(int socket, const void *message, size_t length,
         int flags, const struct sockaddr *dest_addr,
         socklen_t dest_len);
   */

    ret = sendto(s, buf, strlen(buf), 0, (struct sockaddr *)&addr, sizeof(addr));
    if (ret == -1) { perror("sendto"); exit(1); }
  }
 
  return;
}
 
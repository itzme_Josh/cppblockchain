//#ifndef LISTENER_H    // To make sure you don't declare the function more than once by including the header multiple times.
#define LISTENER_H
#define MYPORT "4950" // the port users will be connecting to
#define MAXBUFLEN 100
#include <iostream>
#include <arpa/inet.h>         // for inet_ntop
#include <netdb.h>             // for addrinfo, freeaddrinfo, gai_strerror
#include <netinet/in.h>        // for INET6_ADDRSTRLEN, sockaddr_in, sockadd...
#include <stdio.h>             // for printf, fprintf, perror, NULL, stderr
#include <stdlib.h>            // for exit
#include <string.h>            // for memset
#include <sys/socket.h>        // for sockaddr_storage, bind, socket, AF_INET
#include <unistd.h>            // for close
#include <string>
#include <vector>
#include <poll.h>
#include <thread>
#include "json.hpp"

// for convenience
using json = nlohmann::json;


using namespace std;

std::string getMessage(std::string s){

    auto j3 = json::parse(s);

    std::cout << j3["message"];

    if (j3["message"] == "Update"){

    }else{
        j3.erase("message")
        //add new block
    }
    std::cout <<  j3.dump();

    return j3.dump();

}

void listen() {
   /* int sockfd;
    struct addrinfo hints, *servinfo, *p;
    int rv;
    ssize_t numbytes;
    struct sockaddr_storage their_addr;
    char buf[MAXBUFLEN];
    socklen_t addr_len;
    char s[INET6_ADDRSTRLEN];

    //Sets the first num bytes of the block of memory pointed by hints to the specified value
    //void * memset ( void * ptr, int value, size_t num );
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE; // use my IP */


    // will return information on a particular host name (such as its IP address) 
    //and load up a struct sockaddr for you, taking care of the gritty details (like if it's IPv4 or IPv6.)
    /*
    struct addrinfo {
      int     ai_flags;          // AI_PASSIVE, AI_CANONNAME, ...
      int     ai_family;         // AF_xxx
      int     ai_socktype;       // SOCK_xxx
      int     ai_protocol;       // 0 (auto) or IPPROTO_TCP, IPPROTO_UDP 

      socklen_t  ai_addrlen;     // length of ai_addr
      char   *ai_canonname;      // canonical name for nodename
      struct sockaddr  *ai_addr; // binary address
      struct addrinfo  *ai_next; // next structure in linked list
    };*/

    /*while(true){
        if ((rv = getaddrinfo(NULL, MYPORT, &hints, &servinfo)) != 0) {
            fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
            return;
        }

        // loop through all the results and bind to the first we can
        for (p = servinfo; p != NULL; p = p->ai_next) {

            //p->ai_family is equivalent to (*p).ai_family, i.e. it gets the member called ai_family from the struct that p points to.
            //new socket, (x, ntype of socket, and protocol)
            //see talker.cpp for socket explanation.
            if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) ==
                -1) {
                perror("listener: socket");
                continue;
            }

            //When a remote machine wants to connect to your server program, it needs two pieces of information: the IP address and the port number. The bind() call allows you to do just that.
            //int bind(int sockfd, struct sockaddr *my_addr, socklen_t addrlen);
            if (::bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
                close(sockfd);
                perror("listener: bind");
                continue;
            }

            break;
        }

        //removes servinfo from memory
        freeaddrinfo(servinfo);

        if (p == NULL) {
            fprintf(stderr, "listener: failed to bind socket\n");
            return;
        }

        printf("listener: waiting to recvfrom...\n");

        addr_len = sizeof their_addr;
        if ((numbytes = recvfrom(sockfd, buf, MAXBUFLEN - 1, 0,
                                 (struct sockaddr *)&their_addr, &addr_len)) ==
            -1) {
            perror("recvfrom");
            exit(1);
        }

        //The inet_ntop() function shall convert a numeric address into a text string suitable for presentation. The af argument shall specify the family of the address.
        //printf("listener: got packet from %s\n", inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr *)&their_addr), s, sizeof s));
        printf("listener: packet is %ld bytes long\n", numbytes);
        buf[numbytes] = '\0';
        printf("listener: packet contains \"%s\"\n", buf);

        //do parsing on buf
        close(sockfd);
    }*/
    //return;

        std::cout << "Hello, World" << std::endl;
}


void l (){
    int sockfd;
    struct addrinfo hints, *servinfo, *p;
    int rv;
    ssize_t numbytes;
    struct sockaddr_storage their_addr;
    char buf[MAXBUFLEN];
    socklen_t addr_len;
    char s[INET6_ADDRSTRLEN];

    //Sets the first num bytes of the block of memory pointed by hints to the specified value
    //void * memset ( void * ptr, int value, size_t num );
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE; // use my IP

    while(true){
        if ((rv = getaddrinfo(NULL, MYPORT, &hints, &servinfo)) != 0) {
            fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
            return;
        }

        // loop through all the results and bind to the first we can
        for (p = servinfo; p != NULL; p = p->ai_next) {

            //p->ai_family is equivalent to (*p).ai_family, i.e. it gets the member called ai_family from the struct that p points to.
            //new socket, (x, ntype of socket, and protocol)
            //see talker.cpp for socket explanation.
            if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) ==
                -1) {
                perror("listener: socket");
                continue;
            }

            //When a remote machine wants to connect to your server program, it needs two pieces of information: the IP address and the port number. The bind() call allows you to do just that.
            //int bind(int sockfd, struct sockaddr *my_addr, socklen_t addrlen);
            if (::bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
                close(sockfd);
                perror("listener: bind");
                continue;
            }

            break;
        }

        //removes servinfo from memory
        freeaddrinfo(servinfo);

        if (p == NULL) {
            fprintf(stderr, "listener: failed to bind socket\n");
            return;
        }

        printf("listener: waiting to recvfrom...\n");

        addr_len = sizeof their_addr;
        if ((numbytes = recvfrom(sockfd, buf, MAXBUFLEN - 1, 0,
                                 (struct sockaddr *)&their_addr, &addr_len)) ==
            -1) {
            perror("recvfrom");
            exit(1);
        }

        //The inet_ntop() function shall convert a numeric address into a text string suitable for presentation. The af argument shall specify the family of the address.
        //printf("listener: got packet from %s\n", inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr *)&their_addr), s, sizeof s));
        printf("listener: packet is %ld bytes long\n", numbytes);
        buf[numbytes] = '\0';
        printf("listener: packet contains \"%s\"\n", buf);
        getMessage(buf);
        //do parsing on buf
        close(sockfd);
    }


    std::cout << "Hello, World x" << std::endl;
}

void call_from_thread() {
         std::cout << "Hello, World" << std::endl;
}

int main (){

    std::thread t1(l);
    //std::thread t1(listen);
    t1.join();
    //t1.detach();

//thread t1(listen);

printf("listener: packet contains \"%s\"\n", "helllllo");
return 0;

}
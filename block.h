#pragma once
#include <string>
#include <vector>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "listener.h"
#include <iostream>
#include <sys/time.h>
#include "hash.h"
#include "talker.h"
#include <vector>
#include <string>

#include "json.hpp"

// for convenience
using json = nlohmann::json;

int getDigitCount(int);


struct Block {


    unsigned int index;
    long int previousHash;
    long int timestamp;
    std::string data;
    long int hash;

    Block(unsigned int i, long int b, unsigned int x, const std::string &desc, long int xhash);

    static Block fromJson(const std::string &s);

    static std::vector<Block> blocksFromJson(const std::string &s);

    json toJson() const;

};

Block getGenesisBlock();

void handleBlockchainResponse(const std::string&);

void addNewBlock(const std::string&);

void startUp();

void addNewgeneratedBlock();
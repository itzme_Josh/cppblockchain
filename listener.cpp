/*
** listener.c -- a datagram sockets "server" demo
*/
#include "listener.h"


std::string getMessage(std::string s){

    std::cout << "getMessage" << "\n";
    std::cout << s << "\n";
    auto j3 = json::parse(s);

    std::cout << j3["message"];

    if (j3["message"] == "Update"){

    }
    if (j3["message"] == "AddBlock"){
        j3.erase("message");

        std::cout << "\n" << j3 << "\n";
        std::string lstr = j3.dump();
        addNewBlock(lstr);
    }
    if (j3["message"] == "NewBlock"){
       
        std::cout << "\n" << "NEW BLOCK" << "\n";
        addNewgeneratedBlock();
    }
    //std::cout <<  j3.dump();

    return j3.dump();

}



/*
The <netinet/in.h> header shall define the sockaddr_in structure that includes at least the following members:

sa_family_t     sin_family   AF_INET. 
in_port_t       sin_port     Port number. 
struct in_addr  sin_addr     IP address. 

The sin_port and sin_addr members shall be in network byte order.

The sockaddr_in structure is used to store addresses for the Internet address family. Values of this type shall be cast by applications to struct sockaddr for use with socket functions.
*/

// get sockaddr, IPv4 or IPv6:
static void *get_in_addr(struct sockaddr *sa) {
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in *)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}


void listen() {
    int sockfd;
    struct addrinfo hints, *servinfo, *p;
    int rv;
    ssize_t numbytes;
    struct sockaddr_storage their_addr;
    char buf[MAXBUFLEN];
    socklen_t addr_len;
    char s[INET6_ADDRSTRLEN];

    //Sets the first num bytes of the block of memory pointed by hints to the specified value
    //void * memset ( void * ptr, int value, size_t num );
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE; // use my IP


    // will return information on a particular host name (such as its IP address) 
    //and load up a struct sockaddr for you, taking care of the gritty details (like if it's IPv4 or IPv6.)
    /*
    struct addrinfo {
      int     ai_flags;          // AI_PASSIVE, AI_CANONNAME, ...
      int     ai_family;         // AF_xxx
      int     ai_socktype;       // SOCK_xxx
      int     ai_protocol;       // 0 (auto) or IPPROTO_TCP, IPPROTO_UDP 

      socklen_t  ai_addrlen;     // length of ai_addr
      char   *ai_canonname;      // canonical name for nodename
      struct sockaddr  *ai_addr; // binary address
      struct addrinfo  *ai_next; // next structure in linked list
    };*/

    while(true){
        if ((rv = getaddrinfo(NULL, MYPORT, &hints, &servinfo)) != 0) {
            fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
            return;
        }

        // loop through all the results and bind to the first we can
        for (p = servinfo; p != NULL; p = p->ai_next) {

            //p->ai_family is equivalent to (*p).ai_family, i.e. it gets the member called ai_family from the struct that p points to.
            //new socket, (x, ntype of socket, and protocol)
            //see talker.cpp for socket explanation.
            if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) ==
                -1) {
                perror("listener: socket");
                continue;
            }

            //When a remote machine wants to connect to your server program, it needs two pieces of information: the IP address and the port number. The bind() call allows you to do just that.
            //int bind(int sockfd, struct sockaddr *my_addr, socklen_t addrlen);
            if (::bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
                close(sockfd);
                perror("listener: bind");
                continue;
            }

            break;
        }

        //removes servinfo from memory
        freeaddrinfo(servinfo);

        if (p == NULL) {
            fprintf(stderr, "listener: failed to bind socket\n");
            return;
        }

        printf("listener: waiting to recvfrom...\n");

        addr_len = sizeof their_addr;
        if ((numbytes = recvfrom(sockfd, buf, MAXBUFLEN - 1, 0,
                                 (struct sockaddr *)&their_addr, &addr_len)) ==
            -1) {
            perror("recvfrom");
            exit(1);
        }

        //The inet_ntop() function shall convert a numeric address into a text string suitable for presentation. The af argument shall specify the family of the address.
        printf("listener: got packet from %s\n", inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr *)&their_addr), s, sizeof s));
        printf("listener: packet is %ld bytes long\n", numbytes);
        buf[numbytes] = '\0';
        printf("listener: packet contains \"%s\"\n", buf);

        //do parsing on buf
        close(sockfd);
    }
    return;
}

void n_listen(const char *newPort){

    int sockfd;
    struct addrinfo hints, *servinfo, *p;
    int rv;
    ssize_t numbytes;
    struct sockaddr_storage their_addr;
    char buf[MAXBUFLEN];
    socklen_t addr_len;
    char s[INET6_ADDRSTRLEN];

    //Sets the first num bytes of the block of memory pointed by hints to the specified value
    //void * memset ( void * ptr, int value, size_t num );
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE; // use my IP


    // will return information on a particular host name (such as its IP address) 
    //and load up a struct sockaddr for you, taking care of the gritty details (like if it's IPv4 or IPv6.)
    /*
    struct addrinfo {
      int     ai_flags;          // AI_PASSIVE, AI_CANONNAME, ...
      int     ai_family;         // AF_xxx
      int     ai_socktype;       // SOCK_xxx
      int     ai_protocol;       // 0 (auto) or IPPROTO_TCP, IPPROTO_UDP 

      socklen_t  ai_addrlen;     // length of ai_addr
      char   *ai_canonname;      // canonical name for nodename
      struct sockaddr  *ai_addr; // binary address
      struct addrinfo  *ai_next; // next structure in linked list
    };*/

    std::cout << &newPort;

    while(true){
        if ((rv = getaddrinfo(NULL, newPort, &hints, &servinfo)) != 0) {
            fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
            return;
        }

        // loop through all the results and bind to the first we can
        for (p = servinfo; p != NULL; p = p->ai_next) {

            //p->ai_family is equivalent to (*p).ai_family, i.e. it gets the member called ai_family from the struct that p points to.
            //new socket, (x, ntype of socket, and protocol)
            //see talker.cpp for socket explanation.
            if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) ==
                -1) {
                perror("listener: socket");
                continue;
            }

            //When a remote machine wants to connect to your server program, it needs two pieces of information: the IP address and the port number. The bind() call allows you to do just that.
            //int bind(int sockfd, struct sockaddr *my_addr, socklen_t addrlen);
            if (::bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
                close(sockfd);
                perror("listener: bind");
                continue;
            }

            break;
        }

        //removes servinfo from memory
        freeaddrinfo(servinfo);

        if (p == NULL) {
            fprintf(stderr, "listener: failed to bind socket\n");
            return;
        }

        printf("listener: waiting to recvfrom...\n");

        addr_len = sizeof their_addr;
        if ((numbytes = recvfrom(sockfd, buf, MAXBUFLEN - 1, 0,
                                 (struct sockaddr *)&their_addr, &addr_len)) ==
            -1) {
            perror("recvfrom");
            exit(1);
        }

        //The inet_ntop() function shall convert a numeric address into a text string suitable for presentation. The af argument shall specify the family of the address.
        printf("listener: got packet from %s\n", inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr *)&their_addr), s, sizeof s));
        //printf("listener: packet is %ld bytes long\n", numbytes);
        buf[numbytes] = '\0';
        //printf("listener: packet contains \"%s\"\n", buf);


        getMessage(buf);
        //do parsing on buf
        close(sockfd);
    }
}





/*List all blocks
Create a new block with a content given by the user
List or add peers*/

void listBlocks(){



}


void l (const char *newPort){
    int sockfd;
    struct addrinfo hints, *servinfo, *p;
    int rv;
    ssize_t numbytes;
    struct sockaddr_storage their_addr;
    char buf[MAXBUFLEN];
    socklen_t addr_len;
    char s[INET6_ADDRSTRLEN];

    //Sets the first num bytes of the block of memory pointed by hints to the specified value
    //void * memset ( void * ptr, int value, size_t num );
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE; // use my IP

    while(true){
        if ((rv = getaddrinfo(NULL, MYPORT, &hints, &servinfo)) != 0) {
            fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
            return;
        }

        // loop through all the results and bind to the first we can
        for (p = servinfo; p != NULL; p = p->ai_next) {

            //p->ai_family is equivalent to (*p).ai_family, i.e. it gets the member called ai_family from the struct that p points to.
            //new socket, (x, ntype of socket, and protocol)
            //see talker.cpp for socket explanation.
            if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) ==
                -1) {
                perror("listener: socket");
                continue;
            }

            //When a remote machine wants to connect to your server program, it needs two pieces of information: the IP address and the port number. The bind() call allows you to do just that.
            //int bind(int sockfd, struct sockaddr *my_addr, socklen_t addrlen);
            if (::bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
                close(sockfd);
                perror("listener: bind");
                continue;
            }

            break;
        }

        //removes servinfo from memory
        freeaddrinfo(servinfo);

        if (p == NULL) {
            fprintf(stderr, "listener: failed to bind socket\n");
            return;
        }

        printf("listener: waiting to recvfrom...\n");

        addr_len = sizeof their_addr;
        if ((numbytes = recvfrom(sockfd, buf, MAXBUFLEN - 1, 0,
                                 (struct sockaddr *)&their_addr, &addr_len)) ==
            -1) {
            perror("recvfrom");
            exit(1);
        }

        //The inet_ntop() function shall convert a numeric address into a text string suitable for presentation. The af argument shall specify the family of the address.
        printf("listener: got packet from %s\n", inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr *)&their_addr), s, sizeof s));
        printf("listener: packet is %ld bytes long\n", numbytes);
        buf[numbytes] = '\0';
        printf("listener: packet contains \"%s\"\n", buf);
        getMessage(buf);
        //do parsing on buf
        close(sockfd);
    }


    std::cout << "Hello, World x" << std::endl;
}

int initHttpServer(const std::vector<const char *>& s){

    const char *cstr = s[0];

    cout << "HELLLLLO"<< "\n";
    std::thread t1(l,cstr);
    t1.join();
    
    //use json

    //GET blocks

    //POST next block

    //get peers on network? a list?

    //send to peers on network

    //listen for responses


    /*var app = express();
    app.use(bodyParser.json());
    
    app.get('/blocks', (req, res) => res.send(JSON.stringify(blockchain)));
    app.post('/mineBlock', (req, res) => {
        var newBlock = generateNextBlock(req.body.data);
        addBlock(newBlock);
        broadcast(responseLatestMsg());
        console.log('block added: ' + JSON.stringify(newBlock));
        res.send();
    });
    app.get('/peers', (req, res) => {
        res.send(sockets.map(s => s._socket.remoteAddress + ':' + s._socket.remotePort));
    });
    app.post('/addPeer', (req, res) => {
        connectToPeers([req.body.peer]);
        res.send();
    });
    app.listen(http_port, () => console.log('Listening http on port: ' + http_port));
    };
*/

    return 0;
}

void initP2PServer(std::vector<std::string> s){
    //listener create new socket listening on port for p2p


}
void initConnection(){
    //bind the sockets
}


//this handles the response
void initMessageHandler(){
    //parse json
    //respond based on msg type
    /*
    switch (message.type) {
            case MessageType.QUERY_LATEST:
                write(ws, responseLatestMsg());
                break;
            case MessageType.QUERY_ALL:
                write(ws, responseChainMsg());
                break;
            case MessageType.RESPONSE_BLOCKCHAIN:
                handleBlockchainResponse(message);
                break;
        }
        */

}

void connectToPeers(const std::vector<const char *>& s){

    /*newPeers.forEach((peer) => {
        var ws = new WebSocket(peer);
        ws.on('open', () => initConnection(ws));
        ws.on('error', () => {
            console.log('connection failed')
        });
    });*/
    const char *cstr = s[0];
    cout << s[0]<< "\n";
    n_listen(cstr);

    
    //listentoall(6001, 6006);


    /*for (auto& element : s) {

        //td::string str = "string";

        std::cout << element << "\n";
        const char *cstr = element.c_str();
    
        n_listen(*cstr);

    }*/
    

}


/*
int main (void){
    //listener::listen()
    printf("%s\n", "starting");
    listen();
    return 0;
}*/

//
//  main.cpp
//  
//  Created by Joshua Croft on 20/09/2017.
//


#include "block.h"

// for convenience
using namespace std;

//var initialPeers = process.env.PEERS ? process.env.PEERS.split(',') : [];


    Block::Block(unsigned int i, long int b, unsigned int x, const std::string &desc, long int xhash) : 
    index{i}, previousHash{b}, timestamp{x}, data{desc}, hash{xhash}
    { }


    json Block::toJson() const {
        json ret;
        ret["index"] = index;
        ret["previousHash"] = hash;
        ret["timestamp"] = timestamp;
        ret["data"] = data;
        ret["hash"] = hash;
        return ret;
    }

    Block Block::fromJson(const std::string &s) {
        auto j = json::parse(s);

        cout << "\n" << j["index"] << "\n";
        cout << "\n" << j["previousHash"] << "\n";

        std::string s2 = j["index"];
        unsigned int index = std::stoul(s2);
        s2 = j["previousHash"];
        long int previousHash = std::stol(s2);
        s2 = j["timestamp"];
        long int timestamp = std::stol(s2);
        string data = j["data"];
        s2 = j["hash"];
        long int hash = std::stol(s2);
        

        return *(new Block(index, previousHash, timestamp, data, hash));
    }

    vector<Block> Block::blocksFromJson(const std::string &s){

        vector<Block> temp_blockchainBlocksArray;

        cout << "hey \n";
        //cout << s;

        //json j = s;
        auto j3 = json::parse(s);

        cout << j3;

        cout << j3[0]["index"] << "\n";
        cout << j3[0]["previousHash"] << "\n";
        cout << j3[0]["timestamp"] << "\n";
        cout << j3[0]["data"] << "\n";
        cout << j3[0]["hash"] << "\n";

        for (auto& element : j3) {
            //std::cout << element << '\n';
            //std::cout << element["index"] << '\n';
            //cout << Block::fromJson(j3).index;
            std::string s1 = element.dump();
            //cout << s1 <<"\n";
            //Block x = Block::fromJson(s1);
            temp_blockchainBlocksArray.push_back(Block::fromJson(s1));
        }

        //blockchainBlocksArray.push_back(blockchain);

        return temp_blockchainBlocksArray;
    }


int sockets [0];

enum MessageType { QUERY_LATEST, QUERY_ALL, RESPONSE_BLOCKCHAIN};
vector<Block> blockchainBlocksArray;

Block getGenesisBlock () {
    Block *bl = new Block(0, 0, 1465154705, "my genesis block!!", 9191837481);

    return *bl;
}

Block getLatestBlock(){
    Block var = blockchainBlocksArray.back();
    return var;
}

// main() is where program execution begins.




//const field is sealed cannot modify
int calculateHash(const Block &bl){
    return hashFunc(bl.hash);
}

int calculateHashForBlock(Block bl){
    int newHash = calculateHash(bl);
    return newHash;
}

Block generateNextBlock(string s){
    Block previousBlock = getLatestBlock();

    //previous block?

    json j = previousBlock.toJson();

    cout << "\n" <<"json dump..... " << j.dump() << "\n";

    int nextIndex = previousBlock.index +1;
    
    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int nextTimestamp = tp.tv_sec * 1000 + tp.tv_usec / 1000;

    long int nextHash = calculateHash(previousBlock);

    //Block *bl = new Block(0, "0", 1465154705, "my genesis block!!", 9191837481);
    Block  *bl = new Block(nextIndex, previousBlock.hash, nextTimestamp, s, nextHash);


    return *bl;
    
}

bool isValidNewBlock(Block newBlock, Block previousBlock){
    if (previousBlock.index +1 != newBlock.index){
        return false;

    } else if (previousBlock.hash != newBlock.previousHash){
        return false;
    } else if (calculateHashForBlock(newBlock) != newBlock.hash ){
        return false;
    }

    return true;
}


bool addBlock (Block newBlock){

    if (isValidNewBlock(newBlock, getLatestBlock())){
        blockchainBlocksArray.push_back(newBlock);
        return true;
    }

    return false;
}

void connectToPeers (vector<string> peers){
    // loop through each element in blockchainBlocksArrayay of peers
        // new socket
        // bind socket
}

bool operator!=(Block const& a1, Block const& a2) { 
    if (a1.index != a2.index){
        if (a1.previousHash != a2.previousHash){
            if (a1.timestamp != a2.timestamp){
                if (a1.data != a2.data){
                    if (a1.hash != a2.hash){
                        return true;
                    }
                }
            }
        }
        //return true;
    }
    return false;
}

bool isValidChain(vector<Block> blockchainToValidate){

    Block tbl = *new Block(blockchainToValidate[0].index, blockchainToValidate[0].previousHash, blockchainToValidate[0].timestamp, blockchainToValidate[0].data, blockchainToValidate[0].hash);
    if (blockchainToValidate[0] != getGenesisBlock()) {
        return false;
    }

    vector<Block> tempBlocks;
    tempBlocks.push_back(blockchainToValidate[0]);

    for (int i = 1; i < blockchainToValidate.size(); i++) {
        if (isValidNewBlock(blockchainToValidate[i], tempBlocks[i - 1])) {
            tempBlocks.push_back(blockchainToValidate[i]);
        } else {
            return false;
        }
    }
    return true;

}


void replaceChain (vector<Block> newBlocks){
    if (isValidChain(newBlocks) && newBlocks.size() > blockchainBlocksArray.size()) {
        //console.log('Received blockchain is valid. Replacing current blockchain with received blockchain');
        blockchainBlocksArray = newBlocks;
        //broadcast(responseLatestMsg());
    } else {
        //console.log('Received blockchain invalid');
    }
}

void addNewgeneratedBlock(){
    Block b = generateNextBlock("new block");

    json ji = b.toJson();

    cout << "\n" <<"json dump..... " << ji.dump() << "\n";

    bool goodBlock = addBlock(b);

    json jBlockchain;
    for(auto b : blockchainBlocksArray) {
        auto jb = b.toJson();
        jBlockchain.push_back(jb);
    }

    //cout << goodBlock << "\n";
    printf(goodBlock ? "true" : "false");

    cout << jBlockchain;

    if (goodBlock == true){
        std::string s = b.toJson().dump();
        //sendBlock(s);
    }

}

void addNewBlock(const std::string &s){
    Block nb = Block::fromJson(s);
    bool goodBlock = addBlock(nb);

    //blockchainBlocksArray.push_back(nb);

    json jBlockchain;
    for(auto b : blockchainBlocksArray) {
        auto jb = b.toJson();
        jBlockchain.push_back(jb);
    }

    cout << jBlockchain;

    if (goodBlock == true){
        std::string ss = nb.toJson().dump();
        //sendBlock(ss);
    }
}



void handleBlockchainResponse(string message){
    // get the received blockchain json sort it I assume by index
    //var receivedBlocks = JSON.parse(message.data).sort((b1, b2) => (b1.index - b2.index));
    //var latestBlockReceived = receivedBlocks[receivedBlocks.length - 1];
    //var latestBlockHeld = getLatestBlock();
    //parse json to the blockchainBlocksArrayay...

    //Block b = Block::fromJson(message);

    vector<Block> receivedBlocks = Block::blocksFromJson(message);

    cout <<receivedBlocks[1].index;

    //cout << receivedBlocks[1].index;
    // get last block received

    Block latestBlockReceived = receivedBlocks.back();
    Block lastBlockHeld = getLatestBlock();

    if (latestBlockReceived.index > lastBlockHeld.index){
        blockchainBlocksArray.push_back(latestBlockReceived);
    }else if ( receivedBlocks.size() == 1){

    } else{
        replaceChain(receivedBlocks);
    }
}

void startUp(){
    Block blockchain = getGenesisBlock();
    long int nextHash = calculateHash(blockchain);
    blockchain.hash = nextHash;
    blockchainBlocksArray.push_back(blockchain);

    json js = blockchain.toJson();

    cout << js.dump() << " hey " << "\n";


}

/*
int main() {

    connectToPeers(initialPeers);
initHttpServer();
initP2PServer();

    //json j;
    //convert each struct to json and then pushback to the json s

   
   cout << "Hello World"; // prints Hello World
   Block blockchain = getGenesisBlock();
   blockchainBlocksArray.push_back(blockchain);

    json jBlockchain;
    for(auto b : blockchainBlocksArray) {
        auto jb = b.toJson();
        jBlockchain.push_back(jb);
    }

    cout << jBlockchain.dump();
    cout << blockchain.hash;

    MessageType day = QUERY_LATEST;

    if(day == QUERY_LATEST){
        //cout<<"Ok its Saturday";
    }

    //Block b = Block::fromJson("{\"index\":0, \"previousHash\":0, \"timestamp\":1465154701, \"data\":\"block 0\", \"hash\":919183748}");

    Block b = Block::fromJson("{\"index\":0, \"previousHash\":0, \"timestamp\":1465154701, \"data\":\"block 0\", \"hash\":919183748}");


    cout << "\n";
    cout << b.index << "\n";
    cout << b.previousHash << "\n";
    cout << b.timestamp << "\n";
    cout << b.data << "\n";
    cout << b.hash << "\n";
    cout << "\n";
    //listen();

    //handleBlockchainResponse("[{\"index\":0},{\"index\":1},{\"index\":2},{\"index\":3} ]");

    handleBlockchainResponse("[{\"index\":1, \"previousHash\":1, \"timestamp\":1465154706, \"data\":\"block 1\", \"hash\":9191837482},{\"index\":2, \"previousHash\":2, \"timestamp\":1465154707, \"data\":\"block 2\", \"hash\":9191837483},{\"index\":3, \"previousHash\":3, \"timestamp\":1465154708, \"data\":\"block 3\", \"hash\":9191837484}]");

    



    return 0;
}*/

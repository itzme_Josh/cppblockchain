#include <arpa/inet.h>         // for inet_ntop
#include <netdb.h>             // for addrinfo, freeaddrinfo, gai_strerror
#include <netinet/in.h>        // for INET6_ADDRSTRLEN, sockaddr_in, sockadd...
#include <stdio.h>             // for printf, fprintf, perror, NULL, stderr
#include <stdlib.h>            // for exit
#include <string.h>            // for memset
#include <sys/socket.h>        // for sockaddr_storage, bind, socket, AF_INET
#include <unistd.h>            // for close
#include <iostream>
using namespace std;
int socket;

struct addrinfo{
	int					ai_flags;
	int 				ai_family;
	int 				ai_socktype;
	int 				ai_protocol;
	size_t				ai_addrlen;
	struct sockaddr 	*ai_addr;
	char 				*ai_canonname;			

	struct addrinfo *ai_next; //linked list, next node.
};

struct sockaddr{
	unsigned short		sa_family; //address family, AF_xxx
	char 				sa_family[4]; //14 bytes protocol address
};

struct sockaddr_in{ 
	short int				sin_family; //address family... AF_INET
	unsigned short int		sin_port; //Port number
	struct in_addr			sin_addr; //internet address
	unsigned char			sin_zero[8]; //same size as sockaddr
};


//ip4 only -- see struct in6_addr for IPv6
struct in_addr{
	uint32_t s_add; //that's a 32 bi int (4 bytes)
};

//(IPv6 online -- see struct sockaddr_in and struct in)addr for IPv4)
struct sockaddr_in6 {
	u_int16_t		sin6_family; 	// adress family, AF_INET6
	u_int16_t		sin6_port;		//port numver, Network Byte Order
	u_int16_t		sin6_flowinfo;	//IPv6 flow information
	struct in6_addr	sin6_addr;		//IPv6 address
	u_int32_t		sin6_scope_id; 	//scope id

};

struct sockaddr_storage{
	sa_family_t		ss_family; //address family

	//all is padding, implementation specific, ignore etc.
	char 		__ss_pad1[_SS_PAD1SIZE];
	int64_t		__ss_align;
	char		__ss_pad2[_SS_PAD2SIZE];
};

struct sockaddr_in sa; //IPv4
struct sockaddr_in6 sa6; //IPv6




int main() {

	//convert string ip addresses to binary representations.
	inet_pton(AF_INET, "10.12.110.57", &(sa.sin_addr)); //IPv4
	inet_pton(AF_INET6, "2001:db8:63b3:1::3490", &(sa6.sin6_addr)); // IPv6

	// IPv4:
	char ip4[INET_ADDRSTRLEN];  // space to hold the IPv4 string
	struct sockaddr_in sa;      // pretend this is loaded with something
	inet_ntop(AF_INET, &(sa.sin_addr), ip4, INET_ADDRSTRLEN);
	printf("The IPv4 address is: %s\n", ip4);
	// IPv6:
	char ip6[INET6_ADDRSTRLEN]; // space to hold the IPv6 string
	struct sockaddr_in6 sa6;    // pretend this is loaded with something
	inet_ntop(AF_INET6, &(sa6.sin6_addr), ip6, INET6_ADDRSTRLEN);
	printf("The address is: %s\n", ip6);

	return 0;
}

